<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tarankingtable Model
 *
 * @method \App\Model\Entity\Tarankingtable get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tarankingtable newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tarankingtable[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tarankingtable|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tarankingtable|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tarankingtable patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tarankingtable[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tarankingtable findOrCreate($search, callable $callback = null, $options = [])
 */
class TarankingtableTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tarankingtable');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmpty('ID', 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 15)
            ->requirePresence('Name', 'create')
            ->notEmpty('Name');

        $validator
            ->numeric('Time')
            ->requirePresence('Time', 'create')
            ->notEmpty('Time');

        return $validator;
    }
}
