<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tarankingtable Controller
 *
 * @property \App\Model\Table\TarankingtableTable $Tarankingtable
 *
 * @method \App\Model\Entity\Tarankingtable[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TarankingtableController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tarankingtable = $this->paginate($this->Tarankingtable);

        $this->set(compact('tarankingtable'));
    }

    /**
     * View method
     *
     * @param string|null $id Tarankingtable id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tarankingtable = $this->Tarankingtable->get($id, [
            'contain' => []
        ]);

        $this->set('tarankingtable', $tarankingtable);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tarankingtable = $this->Tarankingtable->newEntity();
        if ($this->request->is('post')) {
            $tarankingtable = $this->Tarankingtable->patchEntity($tarankingtable, $this->request->getData());
            if ($this->Tarankingtable->save($tarankingtable)) {
                $this->Flash->success(__('The tarankingtable has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tarankingtable could not be saved. Please, try again.'));
        }
        $this->set(compact('tarankingtable'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarankingtable id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tarankingtable = $this->Tarankingtable->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarankingtable = $this->Tarankingtable->patchEntity($tarankingtable, $this->request->getData());
            if ($this->Tarankingtable->save($tarankingtable)) {
                $this->Flash->success(__('The tarankingtable has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tarankingtable could not be saved. Please, try again.'));
        }
        $this->set(compact('tarankingtable'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarankingtable id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tarankingtable = $this->Tarankingtable->get($id);
        if ($this->Tarankingtable->delete($tarankingtable)) {
            $this->Flash->success(__('The tarankingtable has been deleted.'));
        } else {
            $this->Flash->error(__('The tarankingtable could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);

    }   
    
      /*リクエストURL
    http://localhost/rankproject/Ranktable/getRankings
    http://localhost/TARankingsystem/Tarankingtable/getranking
                     /ファイル名/テンプレートの中のファイル名

    リクエストパラメータ
    無し
    レスポンスコード
    ランキングデータリスト
    [{"id":2,"name":"\u7530\u4e2d","score":200,"date":"2019-03-20T18:47:48+00:00"},{"id":1,"name":"\u4f50\u85e4","score":100,"date":"2019-03-20T18:47:48+00:00"}]
    */
	//ランキングデータリストを取得する。
    public function getranking()
	{
		$this->autoRender	= false;
		
		//テーブルからランキングリストをとってくる
        $query	= $this->Tarankingtable->find("all");

        //クエリー処理を行う。
        $query->order(['time'=>'asc']);   //昇順
        $query->limit(3);                  //取得件数を3件までに絞る
		
		//jsonにシリアライズする。
		$json	= json_encode($query);

		//jsonデータを返す。（レスポンスとして表示する。）
		echo $json;
    }
            /*
    リクエストURL
    http://localhost/TARankingsystem/Tarankingtable/setranking
    リクエストパラメータ
    name  varchar
    time int
    レスポンスコード
    0:失敗
    1:成功
    */
    //ランキングデータ単体をセットする。
	public function setranking()
	{
		$this->autoRender	= false;

        //POST unityからnameとtimeを受け取る
        $postname		= "";
		if( isset( $this->request->data['Name'] ) ){
			$postname	= $this->request->data['Name'];
			error_log($postname);
		}
		$posttime	= "";
		if( isset( $this->request->data['Time'] ) ){
			$posttime	= $this->request->data['Time'];
			error_log($posttime);
        }
        
        $record = array(
            "Name"=>$postname,
            "Time"=>$posttime,
        );

        //テーブルにレコードを追加
        $prm1    = $this->Tarankingtable->newEntity();
        $prm2    = $this->Tarankingtable->patchEntity($prm1,$record);
        if( $this->Tarankingtable->save($prm2) ){
            echo "レコード追加成功";   //成功
        }else{
            echo "0";   //失敗
        }
	}
}
