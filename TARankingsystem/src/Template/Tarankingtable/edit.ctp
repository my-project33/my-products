<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tarankingtable $tarankingtable
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $tarankingtable->ID],
                ['confirm' => __('Are you sure you want to delete # {0}?', $tarankingtable->ID)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Tarankingtable'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="tarankingtable form large-9 medium-8 columns content">
    <?= $this->Form->create($tarankingtable) ?>
    <fieldset>
        <legend><?= __('Edit Tarankingtable') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('Time');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
