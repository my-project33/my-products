<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tarankingtable $tarankingtable
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tarankingtable'), ['action' => 'edit', $tarankingtable->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tarankingtable'), ['action' => 'delete', $tarankingtable->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $tarankingtable->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Tarankingtable'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tarankingtable'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tarankingtable view large-9 medium-8 columns content">
    <h3><?= h($tarankingtable->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($tarankingtable->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($tarankingtable->ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Time') ?></th>
            <td><?= $this->Number->format($tarankingtable->Time) ?></td>
        </tr>
    </table>
</div>
