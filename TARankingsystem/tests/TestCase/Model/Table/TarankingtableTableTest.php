<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TarankingtableTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TarankingtableTable Test Case
 */
class TarankingtableTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TarankingtableTable
     */
    public $Tarankingtable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tarankingtable'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Tarankingtable') ? [] : ['className' => TarankingtableTable::class];
        $this->Tarankingtable = TableRegistry::getTableLocator()->get('Tarankingtable', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tarankingtable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
