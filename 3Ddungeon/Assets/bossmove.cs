﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossmove : MonoBehaviour
{
    public Animator anm;

    // Start is called before the first frame update
    public void Start()
    {
        anm = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void fireballset()
    {
        anm.SetBool("fire", true);
    }
    public void fireballend()
    {
        anm.SetBool("fire", false);
    }
    public void shockwaveset()
    {
        anm.SetBool("shockwave", true);
    }
    public void shockwavend()
    {
        anm.SetBool("shockwave", false);
    }


}
