﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;     // Json

/// <summary>
/// Json response manager.
/// </summary>
public class RankingDataModel : MonoBehaviour
{


	/// <summary>
	/// Deserialize from json.
	/// MessageData型のリストがjsonに入っていると仮定して
	/// </summary>
	/// <returns>The from json.</returns>
	/// <param name="sStrJson">S string json.</param>
	public static List<Rankingdata> DeserializeFromJson(string sStrJson)
	{
		Debug.Log(sStrJson + "1");
		var ret = new List<Rankingdata>();

		// JSONデータは最初は配列から始まるので、Deserialize（デコード）した直後にリストへキャスト      
		IList jsonList = (IList)Json.Deserialize(sStrJson);

		// リストの内容はオブジェクトなので、辞書型の変数に一つ一つ代入しながら、処理
		foreach (IDictionary jsonOne in jsonList)
		{

			//新レコード解析開始
			var tmp = new Rankingdata();

			if (jsonOne.Contains("Name"))
			{
				tmp.Name = (string)jsonOne["Name"];
			}
			if (jsonOne.Contains("Time"))
			{
				Debug.Log(jsonOne["Time"].GetType().FullName);
				tmp.Time = (long)jsonOne["Time"];
			}


			//現レコード解析終了
			ret.Add(tmp);
			Debug.Log(sStrJson + "1");
		}
		return ret;
	}
}