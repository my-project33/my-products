﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaMusicFadeCross : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource MainBGM;
    public AudioSource BossB;

    private bool isFadeCrossing;
    private bool isInArea;


    private float colliderRadius;　//カプセルのサイズ
    private float areaThreshold; //カプセル　-　0.7の半円
    private float faderCrossDist;　//BGM切り替え区域

    private GameObject player;
    private CapsuleCollider areaCollider;

    void Start()
    {
        GameObject playerObj = GameObject.FindGameObjectWithTag("Player");
        if (playerObj != null)
            player = playerObj;

        areaCollider = GetComponent<CapsuleCollider>();

        colliderRadius = areaCollider.radius;
        areaThreshold = colliderRadius * 0.8f;

        faderCrossDist = colliderRadius - areaThreshold;

        isFadeCrossing = false;
        isInArea = false;

    }

    // Update is called once per frame
    void Update()
    {

        if (isFadeCrossing)
        {
            if (isInArea)
            {
                MainBGM.volume = Mathf.Lerp(MainBGM.volume, 0, 1.5f * Time.deltaTime);
                BossB.volume = Mathf.Lerp(BossB.volume, 0.5f, 1.5f * Time.deltaTime);

            }
                if (!isInArea)
                {
                    MainBGM.volume = Mathf.Lerp(MainBGM.volume, 0.5f, 1.5f * Time.deltaTime);
                    BossB.volume = Mathf.Lerp(BossB.volume, 0f, 3f * Time.deltaTime);

                }
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            isInArea = true;
            BossB.volume = 0.0f;

            isInArea = true;
            isFadeCrossing = true;

            if (!BossB.isPlaying)
                BossB.Play();

            StopCoroutine(DelayTriggerOut());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            isInArea = false;


            StartCoroutine(DelayTriggerOut());
        }
    }
    private IEnumerator DelayTriggerOut()
    {
        yield return new WaitForSeconds(5.0f);

        if (!isInArea)
        {
            isFadeCrossing = false;

            MainBGM.volume = 0.5f;
            BossB.volume = 0.0f;

            BossB.Stop();
        }
    }
}
