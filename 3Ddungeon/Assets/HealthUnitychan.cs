﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUnitychan : MonoBehaviour
{
    public float startingHealth = 100f;
    public Slider healthbarDisplay;
    public GameObject Healthbar;


    private float UnitychanHP;
    //private bool m_Dead;

    void Start()
    {
        Healthbar = GameObject.Find("Healthbar5"); //Sphereというゲームオブジェクトを見つけてballobjectへ格納
    }
    // Start is called before the first frame update
    private void OnEnable()
    {
        UnitychanHP = startingHealth;
       // m_Dead = false;

        SetHealthUi();
    }

    // Update is called once per frame
    private void SetHealthUi()
    {
        // Adjust the value and colour of the slider.
        healthbarDisplay.value = UnitychanHP;


       // fillImage.color = Color.Lerp(zeroHealthColor, fullHealthColor, UnitychanHP / startingHealth);
    }
}
