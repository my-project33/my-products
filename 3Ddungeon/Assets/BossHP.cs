﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHP : MonoBehaviour
{
    public int BossMAXHitpoint = 1000;
    public int BossHitpoint;
    private int count = 0;
    //　HP表示用UI
   // [SerializeField] private GameObject HPUI;
    //　HP表示用スライダー


    private void Start()
    {
        BossHitpoint = BossMAXHitpoint;
    }


    public int GetHp()
    {
        return BossHitpoint;
    }

    public int GetMaxHp()
    {
        return BossMAXHitpoint;
    }

    //　死んだらHPUIを非表示にする
   // public void HideStatusUI()
   // {
   //     HPUI.SetActive(false);
   // }



    //当たり判定メソッド
    private void OnCollisionEnter(Collision collision)
    {
        //衝突したオブジェクトがfireだったとき
        if (collision.gameObject.CompareTag("Fire"))
        {
            count = count + 1;
            Debug.Log("現在のHP(" + BossHitpoint + "回目)");
            BossHitpoint = BossHitpoint - 100;
            if (BossHitpoint <= 0)
            {
              //  HideStatusUI();
                Destroy(this.gameObject);
            }
        }
    }
}
