﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private float CountD = 0f;
    private int second; //タイム整数表示用
    public static long usertime = 0;
    [SerializeField] private Text countdowntext = null;
   // [SerializeField] private GameObject GameClear = default;
    [SerializeField] private GameObject GameOver = default;
    [SerializeField] private GameObject Resetbutton = default;
    public float UnitychanHP = 3;
    public GameObject unichan; //のけぞりアニメ用
    public int mutekiFlag = 0;
    public int mutekiTime = 100;
    public int timeStep = 1;
    [SerializeField] private Text Healthtext = null;

    // Start is called before the first frame update
    void Start()
    {
        unichan = GameObject.Find("unitychan");
        Healthtext.text = "HP:♥ ♥ ♥";
        GameOver.SetActive(false);
        Resetbutton.SetActive(false);
        Time.timeScale = 1f;
        UnitychanHP = 3;
        CountD = 0f;
        usertime = 0;
    }

    // Update is called once per frame
    void Update()
    {//クリアフラグの数をカウントしクリアフラグを取得すると０になりゲームクリア
        int itemcount = GameObject.FindGameObjectsWithTag("Cleaflag").Length;

        if (itemcount <= 0)
        {
            timehokan();
            SceneManager.LoadScene("gameClear");
        }
        countdowntext.text = "現在のタイム" + second.ToString(); ;
        if (itemcount > 0)
        {
            CountD += Time.deltaTime;
            second = (int)CountD;
        }

        if (mutekiFlag == 1) //無敵フラグ　1の間は無敵
        {
            mutekiTime -= timeStep;
            if (mutekiTime < 0)
            {
                mutekiFlag = 0;
                mutekiTime = 100;
            }

        }
    }
    public void TakeDamage()
    {
        if (mutekiFlag == 0)
        {
            mutekiFlag = 1;
            unichan.GetComponent<UnityChan.UnityChanControlScriptWithRgidBody>().DamageHit();
            UnitychanHP -= 1;
            Debug.Log("HPは" + UnitychanHP);
            if(UnitychanHP == 2)
            {
                Healthtext.text = "HP:♥ ♥";
            }
            if (UnitychanHP == 1)
            {
                Healthtext.text = "HP:♥";
            }
        }

        if (UnitychanHP <= 0f)
        {//ゲームオーバーの文字の表示（後でシーンの切り替えにするかも
            //ゲームオーバー時に操作不能にする
            Time.timeScale = 0f;
            GameOver.SetActive(true);
            Resetbutton.SetActive(true);
        }
    }

    public void timehokan()
    {
        //ランキング保管用
        usertime = (int)CountD;
        Debug.Log(usertime);
    }
}
