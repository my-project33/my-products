﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shockwavespawn : MonoBehaviour
{
    [SerializeField] public GameObject player;
    //弾のプレハブオブジェクト
    [SerializeField] public GameObject wave;
    public bool isActive = false;
    [SerializeField] Transform target;
    [SerializeField] private bossmove Bossmove;
    private float targetTime = 5.0f;
    private float currentTime = 0;
    private float Movetaime = 0;
    // Start is called before the first frame update

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isActive = true;
            
            //一秒経つごとに弾を発射する
            currentTime += Time.deltaTime;
            Movetaime += Time.deltaTime;
            if(Movetaime >= 4.5)

            {
                Bossmove.shockwaveset();
            }
            if (targetTime < currentTime)
            {
                currentTime = 0;
                Movetaime = 0;
                //敵の座標を変数posに保存
                // var pos = this.gameObject.transform.position;
                //弾のプレハブを作成
                // var t = Instantiate(tama) as GameObject;
                
                GameObject t = Instantiate(wave);

                //弾のプレハブの位置を敵の位置にする
             //   t.transform.position = pos;
                //敵からプレイヤーに向かうベクトルをつくる
                //プレイヤーの位置から敵の位置（弾の位置）を引く
                //Vector3 vec = player.transform.position - pos;
                //弾のRigidBody2Dコンポネントのvelocityに先程求めたベクトルを入れて力を加える
               // t.GetComponent<Rigidbody>().velocity = vec;
            }
        }
    }

    // Update is called once per frame
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isActive = false;
        }
    }
}
