﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Text
using System;
using UnityEngine.Networking;

public class Rankingmanage : MonoBehaviour
{
    //private GameObject gameObject1;  //inputname格納用
    //private GameObject gameObject2;　//タイム格納用
   // private namehokan script1;
    //private GameManager script2;


    [SerializeField] private Text ranking = null;
    [SerializeField] private Text Nowrecord = null;
    private GameObject GameMnager;

    // Start is called before the first frame update
    private void Awake()
    {
        SetJsonFromWww();
    }
    void Start()
    {

        //gameObject1 = GameObject.Find("InputField");
        //script1 = gameObject1.GetComponent<namehokan>();

        //gameObject2 = GameObject.Find("GameManger");
        //script2 = gameObject2.GetComponent<GameManager>();

        // APIが設置してあるURLパス
        const string url = "http://localhost/TARankingsystem/Tarankingtable/getranking";

        // Wwwを利用して json データ取得をリクエストする
       // SetJsonFromWww();
       StartCoroutine(GetMessages(url, CallbackWwwSuccess, CallbackWwwFailed));

    }


    private void CallbackWwwSuccess(string response)
    {
        // json データ取得が成功したのでデシリアライズして整形し画面に表示する
        List<Rankingdata> rankList = RankingDataModel.DeserializeFromJson(response);

        string sStrOutput = "";
        foreach (Rankingdata ranking in rankList)
        {
            sStrOutput += $"{ranking.Name}さん： Time:{ranking.Time}\n";
            //sStrOutput += $"time:{ranking.time}\n";

        }

        ranking.text = sStrOutput;
        Nowrecord.text = "今回の" + (namehokan.username) + "の記録は" + (GameManager.usertime) + "です";
        
    }

    /// <summary>
    /// Callbacks the www failed.
    /// </summary>
    private void CallbackWwwFailed()
    {
        // jsonデータ取得に失敗した
        ranking.text = "Www Failed";
    }
    private void CallbackApiSuccess(string response)
    {
        // json データ取得が成功したのでデシリアライズして整形し画面に表示する
        //ranking.text = response;
    }

    private IEnumerator GetMessages(string url, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        // WWWを利用してリクエストを送る
        var webRequest = UnityWebRequest.Get(url);

        //タイムアウトの指定
        webRequest.timeout = 5;

        // WWWレスポンス待ち
        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(webRequest.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            // リクエスト成功の場合
            Debug.Log($"Success:{webRequest.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
    }
    private IEnumerator ResponseCheckForTimeOutWWW(UnityWebRequest webRequest, float timeout)
    {
        float requestTime = Time.time;

        while (!webRequest.isDone)
        {
            if (Time.time - requestTime < timeout)
            {
                yield return null;
            }
            else
            {
                Debug.LogWarning("TimeOut"); //タイムアウト
                break;
            }
        }

        yield return null;
    }
    private void SetJsonFromWww()
    {
        // APIが設置してあるURLパス
        string sTgtURL = "http://localhost/TARankingsystem/Tarankingtable/setranking";

        //string username1 = script1.username;
        //float counttime = script2.usertime;
        //Debug.Log(username1);
       // Debug.Log(counttime);
        
        string name = namehokan.username;
        long time = GameManager.usertime;
        

        //string name = "mosumosu22";
        //float time = 150.12f;
        Debug.Log(name + "今回の名前");                                                                                              
        Debug.Log(time + "今回の記録");

        // Wwwを利用して json データ取得をリクエストする
        StartCoroutine(SetMessage(sTgtURL, name, time, CallbackApiSuccess, CallbackWwwFailed));

    }

    private IEnumerator SetMessage(string url, string name, long time, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("Name", name);
        form.AddField("Time", time.ToString());

        // WWWを利用してリクエストを送る
        UnityWebRequest webRequest = UnityWebRequest.Post(url, form);

        //タイムアウトの指定
        webRequest.timeout = 5;

        // WWWレスポンス待ち
        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(webRequest.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {



            // リクエスト成功の場合
            Debug.Log($"Success:{webRequest.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
    }
    private void webRequestSuccess(string response)
    {
        ranking.text = response;
    }

    // Update is called once per frame

}

