﻿using System.Collections.Generic;
using UnityEngine;

public class ZombieHP : MonoBehaviour
{
    private float ZombieHitpoint = 100;
    private int count = 0;
    private GameObject gamemanager;

    private void Start()
    {
        gamemanager = GameObject.Find("GameManager");
    }
    //当たり判定メソッド
    private void OnCollisionEnter(Collision collision)
    {
        //衝突したオブジェクトがBullet(大砲の弾)だったとき
        if (collision.gameObject.CompareTag("Fire"))
        {
            count = count + 1;
            Debug.Log("敵と弾が衝突しました！！！("+count+"回目)");
            ZombieHitpoint = ZombieHitpoint - 50;
            if (ZombieHitpoint <= 0)
               {
                Destroy(this.gameObject);
                }
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            gamemanager.GetComponent<GameManager>().TakeDamage();
        }
    }
}