﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBalltake : MonoBehaviour
{
   // private GameObject boss;
    //プレイヤーオブジェクト
    [SerializeField] public GameObject player;
    //弾のプレハブオブジェクト
    [SerializeField] public GameObject tama;
    public bool isActive = false;
    [SerializeField] Transform target;
    [SerializeField] private bossmove Bossmove;

    //一秒ごとに弾を発射するためのもの
    public float targetTime = 3.0f;
    public float currentTime = 0;
    public float Movetime = 0f;

    // Update is called once per frame
    public void Start()
    {
        //Bossmove = GetComponent<bossmove>();
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isActive = true;

            //一秒経つごとに弾を発射する
            currentTime += Time.deltaTime;
            Movetime += Time.deltaTime;
            if(Movetime >= 2.8)
            {
                Bossmove.fireballset();
            }
            if (targetTime < currentTime)
            {
                currentTime = 0;
                Movetime = 0;
                //敵の座標を変数posに保存
                var pos = this.gameObject.transform.position;

                //弾のプレハブを作成
                // var t = Instantiate(tama) as GameObject;
                GameObject t = Instantiate(tama);
                //弾のプレハブの位置を敵の位置にする
                t.transform.position = pos;
                //敵からプレイヤーに向かうベクトルをつくる
                //プレイヤーの位置から敵の位置（弾の位置）を引く
                Vector3 vec = player.transform.position - pos;
                //弾のRigidBody2Dコンポネントのvelocityに先程求めたベクトルを入れて力を加える
                t.GetComponent<Rigidbody>().velocity = vec;
               
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isActive = false;
        }
    }
}