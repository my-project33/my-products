﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    public LayerMask unitychan;
    [SerializeField] public float Attacktime = 3f;
    [SerializeField] public float Radius = 5f; //吹き飛ばす力
    public float MAXdamege = 1f;
    public float ExplosionForce = 1000f;
    public ParticleSystem m_ExplosionParticles; //爆風格納用

    public GameObject gamemanager;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject,Attacktime);
        gamemanager = GameObject.Find("GameManager");
    }

    [System.Obsolete]
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            gamemanager.GetComponent<GameManager>().TakeDamage();
        }

            //gamemanager.GetComponent<GameManager>().TakeDamage();
        /*    Collider[] colliders = Physics.OverlapSphere(transform.position, Radius, unitychan);
        for (int i = 0; i < colliders.Length; i++)
        {
           
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

          
            if (!targetRigidbody)
                continue;

            targetRigidbody.AddExplosionForce(ExplosionForce, transform.position, Radius);

            GameManager UnitychanHP = targetRigidbody.GetComponent<GameManager>();


            if (!UnitychanHP)
                continue;




            UnitychanHP.TakeDamage();

        }
        */
        m_ExplosionParticles.transform.parent = null;

        m_ExplosionParticles.Play();

        Destroy(m_ExplosionParticles.gameObject, m_ExplosionParticles.duration);
        Destroy(gameObject);
    }
    // ダメージ量の計算はしない　当たれば１
   /* private float CalculateDamage(Vector3 targetPosition)
    {
        // Create a vector from the shell to the target.
        Vector3 explosionToTarget = targetPosition - transform.position;

        // Calculate the distance from the shell to the target.
        float explosionDistance = explosionToTarget.magnitude;

        // Calculate the proportion of the maximum distance (the explosionRadius) the target is away.
        float relativeDistance = (Radius - explosionDistance) / Radius;

        // Calculate damage as this proportion of the maximum possible damage.
        float damage = relativeDistance * MAXdamege;

        // Make sure that the minimum damage is always 0.
        damage = Mathf.Max(0f, damage);

        return damage;

    }*/
}
