﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class startbutton : MonoBehaviour
{
    [SerializeField] private Text Nameinput;
    [SerializeField] private Button btn;

    private void Start()
    {
        Button btn = GetComponent<Button>();
        btn.interactable = false;
    }
    private void Update()
    {
        if(namehokan.username == "")
        {
            btn.interactable = false;
            Nameinput.text = "名前を入力してください";
        }
        else
        {
            btn.interactable = true;
            Nameinput.text = "入力を終えたら\nNewGameボタンでスタート";
        }
    }
    public void LoadingNewScene()
    {
        SceneManager.LoadScene("main");

    }

    // Start is called before the first frame update

}
