﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionShockwave : MonoBehaviour
{

    private GameManager gamemanager;
    private CapsuleCollider capsulecollider;

    // Use this for initialization
    void Start()
    {
        gamemanager = GetComponent<GameManager>();
        capsulecollider = GetComponent<CapsuleCollider>();
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider.tag == "Shockwave")
        {
            gamemanager.TakeDamage();
            Physics.IgnoreCollision(capsulecollider, hit.collider, true);
            Debug.Log("接触");
        }
    }
}