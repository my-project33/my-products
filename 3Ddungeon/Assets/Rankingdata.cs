﻿public class Rankingdata
{
    public string Name { get; set; }
    public long Time { get; set; }

    public Rankingdata()
    {
        Name = "";
        Time = default;
    }
}