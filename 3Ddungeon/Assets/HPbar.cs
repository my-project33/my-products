﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HPbar : MonoBehaviour
{
    public Slider BossHPslider;
    private GameObject boss;
    public int HP;
    public int MAXHP;
    
    // Start is called before the first frame update
    void Start()
    {
        boss = GameObject.Find("Boss");

    }

    // Update is called once per frame
    void Update()
    {
        HP = boss.GetComponent<BossHP>().BossHitpoint;
        MAXHP = boss.GetComponent<BossHP>().BossMAXHitpoint;
        BossHPslider.maxValue = MAXHP;
        BossHPslider.value = HP;
    }
}
